$(document).ready(function() {

  selectStyler();
  toggleSelect('.select ul', '.select span');
  toggleActive('.lk-logo, .has-dropdown', '.nav-header, .has-dropdown > a');
  resizeObj();
  $(window).load(function() {
    var top = $(window).scrollTop();
    $('html').animate({
      scrollTop: top + 1
    })
  });

  $('.main-nav li ul').parent().addClass('has-dropdown');

  $(document).on('click', '.has-dropdown>a', function(e) {
    e.preventDefault();
    var width = $(window).width();
    if(width<=991) {
      $(this).parent().toggleClass('active').parent().find('>li').not($(this).parent()).removeClass('active');
    }
    // $(this).next('ul').slideToggle('fast');
  });

  $(document).animateScroll();
  var doc = document.documentElement;
  doc.setAttribute('data-useragent', navigator.userAgent);

  $('.boxloader-y').boxLoader({
    direction:"y",
    position: "50%",
    effect: "fadeIn",
    duration: "1.5s",
    windowarea: "50%"
  });
  $('.boxloader-x1').boxLoader({
    direction:"x",
    position: "-50%",
    effect: "fadeIn",
    duration: "1.5s",
    windowarea: "50%"
  });
  $('.boxloader-x2').boxLoader({
    direction:"x",
    position: "50%",
    effect: "fadeIn",
    duration: "1.5s",
    windowarea: "50%"
  });

  

  var active_tab = $('.tabs-nav').find('.active').find('a').attr('href');
  $(active_tab).show('fast');
  $(document).on('click', '.tabs-nav a', function(e) {
    e.preventDefault();
    var tab = $(this).attr('href');
    $(this).parent().addClass('active').parent().find('li').not($(this).parent()).removeClass('active');
    $('.tab').not($(tab)).slideUp('fast');
    $(tab).slideDown('fast');
  });

  $('.tab-nav a').click(function(e) {
    e.preventDefault();
    var tab = $(this).attr('href');
    var obj = $(tab);
    obj.slideDown('fast').parent().find('.tab').not(obj).slideUp('fast');
    $(this).parent().addClass('active');
    $(this).parents('.tab-nav').find('li').not($(this).parent()).removeClass('active');
  })

 /* $(window).load(function() {
    $('.tabs-block').css('min-height', $('.tabs-block').height());
  });*/

 

  $(document).on('click', '.nav-header', function() {
    // $(this).toggleClass('active');
    $(this).toggleClass('active');
    $('.main-nav').slideToggle('fast');
  });

  $('.faq-list > li > a').click(function(e) {
    e.preventDefault();
    $(this).parent().toggleClass('active').parent().find('>li').not($(this).parent()).removeClass('active').find('.answer-block').slideUp('fast');
    $(this).next('.answer-block').slideToggle('fast');
  })

});



function selectStyler() {
  $('select').each(function() {
    $(this).hide();
    var __this = $(this);
    var selected = $(this).find('option:selected');
    var opt_length = $(this).find('option').length;
    var li = '';
    if(opt_length) {
      for (var i = 0; i < opt_length; i++) {
        var active = '';
        if(selected.index()==i) {
          active = 'class="active"';
        }
        li += '<li data-value='+__this.find('option').eq(i).val()+' '+active+'>' + __this.find('option').eq(i).text(); + '<li>';
      }
    }

    var cont = '<div class="select"><span>'+selected.val()+'</span><ul class="reset-list" style="display:none;">'+li+'</ul></div>';
    __this.before(cont);
  });
  $('.select span').click(function() {
    $(this).next('ul').slideToggle('fast');
  });
  $('.select li').click(function() {
    var id = $(this).index();
    $(this).parents('.select').next('select').find('option').eq(id).prop('selected', true);
    $(this).parent().prev('span').text($(this).text());
  });
}
function toggleSelect(select,link) {
  $(window).click(function(e) {
    if ($(e.target).closest(select+','+link).length === 0) {
      $(select).slideUp('fast');
    }
  });
}
function toggleActive(active,link) {
  $(window).click(function(e) {
    if ($(e.target).closest(active+','+link).length === 0) {
      $(active).removeClass('active');
    }
  });
}

function resizeObj() {
  var width = $(window).width();
  if(width>991) {
    $('.main-nav').removeAttr('style');
    $('.nav-header').removeClass('active');
  }

  if(width<768) {
    var contacts = $('.header-contacts').clone();
    var langs = $('.lang-nav').clone();
    if($('.header-contacts-clone').length == 0) {
      $('.main-nav').prepend(contacts.addClass('header-contacts-clone clone'));
      $('.main-nav').append(langs.addClass('lang-nav-clone clone'));
    }
  }else {
    $('.clone').remove();
  }

  $(window).resize(function() {
    width = $(window).width();
    if(width>991) {
      $('.main-nav').removeAttr('style');
      $('.nav-header').removeClass('active');
    } 

    if(width<768) {
      contacts = $('.header-contacts').clone();
      langs = $('.lang-nav').clone();
      if($('.header-contacts-clone').length == 0) {
        $('.main-nav').prepend(contacts.addClass('header-contacts-clone clone'));
        $('.main-nav').append(langs.addClass('lang-nav-clone clone'));
      }
    }else {
      $('.clone').remove();
    }

  })
}